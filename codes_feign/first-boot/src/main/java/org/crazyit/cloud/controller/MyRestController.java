package org.crazyit.cloud.controller;

import org.crazyit.cloud.vo.Person;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyRestController {
	/**
	 * produces = MediaType.APPLICATION_JSON_VALUE
	 * 表示返回一个Gson对象,feign调用者需要一个gson解码器
	 * 接收对象并进行解码
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/person/{id}", method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Person getPerson(@PathVariable Integer id) {
		Person p = new Person();
		p.setId(id);
		p.setName("angus");
		p.setAge(30);
		return p;
	}

	/**
	 * 表示只接收json对象，
	 * feign调用需加上json编码器
	 * @param p
	 * @return
	 */
	@RequestMapping(value = "/person/create", method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public Person createPerson(@RequestBody Person p) {
		System.out.println(p.getName() + "---" + p.getAge());
		return p;
//		return "success, id: " + p.getId();
	}

	/**
	 * 参数与返回值均为XML
	 */
	@RequestMapping(value = "/person/createXML", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_XML_VALUE,
			produces = MediaType.APPLICATION_XML_VALUE)
	public String createXMLPerson(@RequestBody Person person) {
		System.out.println(person.getName() + "-" + person.getId());
		return "<result><message>success</message></result>";
	}

}
