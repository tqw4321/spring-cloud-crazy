package org.crazyit.cloud.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 * 请求拦截器
 */
public class MyInterceptor implements RequestInterceptor {
    /**
     * 这里可以对请求进行拦截，设置修改信息
     * 比如请求头
     * 比如用户名密码等信息
     * @param requestTemplate
     */
    public void apply(RequestTemplate requestTemplate) {
        System.out.println("请求拦截器");
    }
}
