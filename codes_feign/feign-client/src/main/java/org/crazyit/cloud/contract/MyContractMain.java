package org.crazyit.cloud.contract;

import feign.Contract;
import feign.Feign;
import feign.MethodMetadata;
import org.crazyit.cloud.HelloClient;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * 注解翻译器
 */
public class MyContractMain  {

    public static void main(String[] args) {
        ContractClient client = Feign.builder()
                .contract(new MyContract())
                .target(ContractClient.class,
                "http://localhost:8080");
        String result = client.hello();
        System.out.println(result);
    }
}
