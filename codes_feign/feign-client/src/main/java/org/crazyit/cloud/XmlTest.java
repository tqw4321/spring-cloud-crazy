package org.crazyit.cloud;

import feign.Feign;
import feign.jaxb.JAXBContextFactory;
import feign.jaxb.JAXBDecoder;
import feign.jaxb.JAXBEncoder;

public class XmlTest {

	public static void main(String[] args) {
		JAXBContextFactory jaxbFactory = new JAXBContextFactory.Builder().build();
		// 获取服务接口
		HelloClient client = Feign.builder()
				.encoder(new JAXBEncoder(jaxbFactory))
				.decoder(new JAXBDecoder(jaxbFactory))
				.target(HelloClient.class, "http://localhost:8080/");
		// 构建参数
		PersonXml p = new PersonXml();
		p.setId(1);
		p.setName("angus");
		p.setAge(33);
		// 调用接口并返回结果
		Result result = client.createXMLPerson(p);
		System.out.println(result.getMessage());
	}

}
