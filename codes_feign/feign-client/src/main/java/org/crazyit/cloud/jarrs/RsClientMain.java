package org.crazyit.cloud.jarrs;

import feign.Feign;
import feign.jaxrs.JAXRSContract;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

public class RsClientMain {
    /**
     * 注解翻译器
     * 将jaxrs的注解翻译成feign的注解
     * JAXRSContract()
     * @param args
     */
    public static void main(String[] args) {
        RsClient client = Feign.builder()
                .contract(new JAXRSContract())
                .target(RsClient.class,
                        "http://localhost:8080");
        String result = client.hello();
        System.out.println(result);
    }
}
