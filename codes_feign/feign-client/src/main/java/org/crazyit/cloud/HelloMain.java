package org.crazyit.cloud;

import feign.Feign;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;

public class HelloMain {

	public static void main(String[] args) {
		hello();
		createPerson();
	}

	/**
	 * 发送字符串请求，返回也是字符串
	 */
	public static  void hello(){
		HelloClient client = Feign.builder().target(HelloClient.class,
				"http://localhost:8080");
		String result = client.hello();
		System.out.println(result);
	}

	/**
	 * 将对象编码成json发送出去,返回Json
	 */
	public static void createPerson(){
		HelloClient client = Feign.builder()
				.encoder(new GsonEncoder())
				.decoder(new GsonDecoder())
				.target(HelloClient.class,"http://localhost:8080");

		Person person = new Person();
		person.setAge("10");
		person.setId(12);
		person.setName("tony");
		Person value = client.createPerson(person);
		System.out.println(value);
	}



}
