package org.crazyit.cloud.contract;

import feign.Contract;
import feign.MethodMetadata;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * 注解翻译器,自定一个方法级别的翻译器
 */
public class MyContract extends Contract.BaseContract {

    protected void processAnnotationOnClass(MethodMetadata methodMetadata, Class<?> aClass) {

    }

    protected void processAnnotationOnMethod(MethodMetadata methodMetadata, Annotation annotation, Method method) {
        if(MyUrl.class.isInstance(annotation)){
            MyUrl myUrl = method.getAnnotation(MyUrl.class);
            String url = myUrl.url();
            String methods = myUrl.method();
            methodMetadata.template().method(methods).append(url);
        }
    }

    protected boolean processAnnotationsOnParameter(MethodMetadata methodMetadata, Annotation[] annotations, int i) {
        return false;
    }
}
