package org.crazyit.cloud.log;

import feign.Feign;
import feign.Logger;
import org.crazyit.cloud.HelloClient;
import org.crazyit.cloud.interceptor.MyInterceptor;

/**
 * 打印调用日志
 */
public class LogMain {
    public static void main(String[] args) {
        HelloClient client = Feign.builder()
                .logLevel(Logger.Level.FULL)
                .logger(new Logger.JavaLogger().appendToFile("logs/log.txt"))
                .requestInterceptor(new MyInterceptor())
                .target(HelloClient.class,
                        "http://localhost:8080");
        String result = client.hello();
        System.out.println(result);
    }
}
