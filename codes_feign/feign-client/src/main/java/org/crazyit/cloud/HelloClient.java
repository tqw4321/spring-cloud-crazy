package org.crazyit.cloud;

import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface HelloClient {

	@RequestLine("GET /hello")
	public String hello();
	
	@RequestLine("GET /person/{id}")
	public Person getPerson(@Param("id") Integer id);

	/**
	 * 发送一个post请求，并且参数是json类型
	 * @param person
	 * @return
	 */
	@RequestLine(("POST /person/create"))
	@Headers("Content-Type: application/json")
	public Person createPerson(Person person);

	@RequestLine(("POST /person/createXML"))
	@Headers("Content-Type: application/xml")
	public Result createXMLPerson(PersonXml person);
}
